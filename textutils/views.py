from django import http
from django.http import HttpResponse
from django.shortcuts import render

def documentslink(request):
    return HttpResponse('''<h1>DJANGO DOC</h1> <a href = " https://docs.djangoproject.com/en/3.2/">django doc</a> <br><a href = "https://docs.djangoproject.com/en/3.2/intro/">django chapters</a>''')

def contact(request):
    return render(request, 'contact.html', {'contact_no': '7281028035'})
    
def about(request):
    return render(request, 'about.html', {'about': 'this is text analyze web app'})
    
def index(request):
    return render(request, 'index.html')

def analyze(request):
    #get the text
    data = request.GET.get('text', 'default')
    #check checkbox values on/off
    remove_punc = request.GET.get('removepunc', 'off')
    full_capitalize = request.GET.get('capitalize', 'off') 
    new_line_remover = request.GET.get('newlineremover', 'off') 
    extra_space_remover = request.GET.get('extraspaceremover', 'off')
    #check checkbox which checkbox is on
    if remove_punc == 'on':
        punctuations_list = '''!()-[]{};:'"|/<>,./?@#$#%^&*_~`'''
        analyzed = ''
        for  char in data:
            if char not in punctuations_list:
                analyzed = analyzed + char
        params = {'purpose': 'Remove Punctuations', 'analyze_text': analyzed}
        #analyze the text
        return render(request, 'analyze.html', params)
    
    elif (full_capitalize == 'on'):
        analyzed = ''
        for char in data:
            analyzed = analyzed + char.upper()   
            print(analyzed)
        params = {'purpose': 'CAPITALIZED', 'analyze_text': analyzed}
        #analyze the text
        return render(request, 'analyze.html', params)
    
    elif (new_line_remover == 'on'):
        analyzed = ''
        for char in data:
            if char !='\n':
                analyzed = analyzed + char 
        params = {'purpose': 'new Line Remover', 'analyze_text': analyzed}
        #analyze the text
        return render(request, 'analyze.html', params)
    
    elif (extra_space_remover == 'on'):
        analyzed = ''
        for index, char in enumerate(data):
            if not data[index] == ' ' and data[index+1] == ' ':
                analyzed = analyzed + char 
        params = {'purpose': 'new Line Remover', 'analyze_text': analyzed}
        #analyze the text
        return render(request, 'analyze.html', params)
    
    else:
        return HttpResponse('error')